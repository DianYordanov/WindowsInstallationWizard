# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QGridLayout, QHBoxLayout, QLabel,
    QLayout, QPushButton, QSizePolicy, QSpacerItem,
    QWidget)

class Ui_Widget1(object):
    def setupUi(self, Widget1):
        if not Widget1.objectName():
            Widget1.setObjectName(u"Widget1")
        Widget1.resize(800, 600)
        Widget1.setMinimumSize(QSize(800, 600))
        Widget1.setStyleSheet(u"")
        self.horizontalLayoutWidget = QWidget(Widget1)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 290, 811, 51))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(70, 0, 70, 0)
        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout.addItem(self.verticalSpacer_3)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout.addItem(self.verticalSpacer_2)

        self.Setup = QPushButton(self.horizontalLayoutWidget)
        self.Setup.setObjectName(u"Setup")

        self.horizontalLayout.addWidget(self.Setup)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout.addItem(self.verticalSpacer)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout.addItem(self.verticalSpacer_4)

        self.horizontalLayoutWidget_2 = QWidget(Widget1)
        self.horizontalLayoutWidget_2.setObjectName(u"horizontalLayoutWidget_2")
        self.horizontalLayoutWidget_2.setGeometry(QRect(0, 120, 801, 80))
        self.horizontalLayout_2 = QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalSpacer_6 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout_2.addItem(self.verticalSpacer_6)

        self.which = QLabel(self.horizontalLayoutWidget_2)
        self.which.setObjectName(u"which")

        self.horizontalLayout_2.addWidget(self.which)

        self.verticalSpacer_5 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.horizontalLayout_2.addItem(self.verticalSpacer_5)

        self.gridLayoutWidget = QWidget(Widget1)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(0, 340, 801, 111))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.button_manual = QPushButton(self.gridLayoutWidget)
        self.button_manual.setObjectName(u"button_manual")

        self.gridLayout.addWidget(self.button_manual, 0, 3, 1, 1)

        self.Scriptless = QPushButton(self.gridLayoutWidget)
        self.Scriptless.setObjectName(u"Scriptless")

        self.gridLayout.addWidget(self.Scriptless, 2, 3, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 0, 4, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 0, 0, 1, 1)

        self.button_christitustech = QPushButton(self.gridLayoutWidget)
        self.button_christitustech.setObjectName(u"button_christitustech")

        self.gridLayout.addWidget(self.button_christitustech, 2, 1, 1, 1)

        self.button_script = QPushButton(self.gridLayoutWidget)
        self.button_script.setObjectName(u"button_script")

        self.gridLayout.addWidget(self.button_script, 0, 1, 1, 1)


        self.retranslateUi(Widget1)

        QMetaObject.connectSlotsByName(Widget1)
    # setupUi

    def retranslateUi(self, Widget1):
        Widget1.setWindowTitle(QCoreApplication.translate("Widget1", u"Widget", None))
        self.Setup.setText(QCoreApplication.translate("Widget1", u"Setup", None))
        self.which.setText(QCoreApplication.translate("Widget1", u"Which installation method do you choose? ", None))
        self.button_manual.setText(QCoreApplication.translate("Widget1", u"Manual installation", None))
        self.Scriptless.setText(QCoreApplication.translate("Widget1", u"Scriptless executables", None))
        self.button_christitustech.setText(QCoreApplication.translate("Widget1", u"christitustech-winutil", None))
        self.button_script.setText(QCoreApplication.translate("Widget1", u"Automatic script", None))
    # retranslateUi

