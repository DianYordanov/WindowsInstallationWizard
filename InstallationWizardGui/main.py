import sys
import os
import subprocess
import winreg
from PySide6.QtWidgets import *
from PySide6.QtCore import Slot, QSize,QFile, Qt, QTimer
from PySide6.QtUiTools import QUiLoader
from PySide6.QtGui import QPixmap, QIcon, QWindow, QColor, QPaintEvent, QFont, QPainter

# venv\Scripts\activate.bat

# pyside6-uic ui_files/form.ui -o ui_py_files/ui_form.py
from ui_py_files.ui_form import Ui_Widget1

# pyside6-uic ui_files/ChristitustechWindow.ui -o ui_py_files/Ui_ChristitustechWindow.py
from ui_py_files.ui_ChristitustechWindow import Ui_ChristitustechWindow

# pyside6-uic ui_files/ScriptInstallationWindow.ui -o ui_py_files/Ui_ScriptInstallationWindow.py
from ui_py_files.ui_ScriptInstallationWindow import Ui_ScriptInstallationWindow

# pyside6-uic ui_files/ManualInstallationWindow.ui -o ui_py_files/Ui_ManualInstallationWindow.py
from ui_py_files.ui_ManualInstallationWindow import Ui_ManualInstallationWindow

# pyside6-uic ui_files/ChocoWindow.ui -o ui_py_files/Ui_ChocoWindow.py
from ui_py_files.ui_ChocoWindow import Ui_ChocoWindow

# pyside6-uic ui_files/WingetWindow.ui -o ui_py_files/Ui_WingetWindow.py
from ui_py_files.ui_WingetWindow import Ui_WingetWindow

# pyside6-uic ui_files/ScriptChoiceWindowForChocoAndWinget.ui -o ui_py_files/Ui_ScriptChoiceWindowForChocoAndWinget.py
from ui_py_files.ui_ScriptChoiceWindowForChocoAndWinget import Ui_ScriptChoiceWindowForChocoAndWinget

# pyside6-uic ui_files/ManualChoiceWindow.ui -o ui_py_files/Ui_ManualChoiceWindow.py
from ui_py_files.ui_ManualChoiceWindow import Ui_ManualChoiceWindow

# pyside6-uic ui_files/UnifiedScriptInstall.ui -o ui_py_files/Ui_UnifiedScriptInstall.py
from ui_py_files.ui_UnifiedScriptInstall import Ui_UnifiedScriptInstall

# pyside6-uic ui_files/Scriptless.ui -o ui_py_files/ui_Scriptless.py
from ui_py_files.ui_Scriptless import Ui_Scriptless

# from classes.test import HelloWorld

# from PIL import Image
import classes.icon_extraction_lib

# pyinstaller widget.py
# pyinstaller --noconsole main.py && robocopy %cd%\windows-executables %cd%\dist\main\windows-executables /E
# pyinstaller --noconsole --onefile main.py
# pyinstaller --noconsole --onefile --icon=Icon\OIG3256x256.ico main.py
# pyinstaller --noconsole --onefile --icon="OIG3256x256.ico" main.py
# pyinstaller  --clean --noconsole --onefile -i="OIG3256x256.ico" main.py
# pyinstaller  --clean --noconsole --onefile --icon=Icon\OIG3256x256.ico main.py

# for the links:
# pyinstaller --noconsole --onefile exeLinkToBuildScript.py
# pyinstaller --noconsole --onefile exeLinkToChocoCreateScript.py
# pyinstaller --noconsole --onefile exeLinkToChocoInstallScript.py
# pyinstaller --noconsole --onefile exeLinkToRunBuild.py
# pyinstaller --noconsole --onefile exeLinkToWingetCreateScript.py
# pyinstaller --noconsole --onefile exeLinkToWingetInstallScript.py

from Neumorphism.Neumorphism import *

class ManualChoiceWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ManualChoiceWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/ManualChoiceWindow.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.go_back)
        self.ui2.BuildScript.clicked.connect(self.go_BuildScriptInstallationWindow)
        self.ui2.RunBuild.clicked.connect(self.go_RunBuildInstallationWindow)

    def go_back(self):
        self.hide()
        if self.w is None:
            self.w = Widget()
        self.w.show()

    def go_BuildScriptInstallationWindow(self):
        self.hide()
        if self.w is None:
            self.w = BuildScriptInstallationWindow()
        self.w.show()

    def go_RunBuildInstallationWindow(self):
        self.hide()
        if self.w is None:
            self.w = RunBuildInstallationWindow()
        self.w.show()

class ScriptChoiceWindowForChocoAndWinget(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ScriptChoiceWindowForChocoAndWinget()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/ScriptChoiceWindowForChocoAndWinget.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.go_back)
        self.ui2.Choco.clicked.connect(self.go_to_choco_install_window)
        self.ui2.Winget.clicked.connect(self.go_to_winget_install_window)

    def go_back(self):
        self.hide()
        if self.w is None:
            self.w = Widget()
        self.w.show()

    def go_to_choco_install_window(self):
        self.hide()
        if self.w is None:
            self.w = ChocoWindow()
        self.w.show()

    def go_to_winget_install_window(self):
        self.hide()
        if self.w is None:
            self.w = WingetWindow()
        self.w.show()

class WingetWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_WingetWindow()
        self.ui2.setupUi(self)

        light_outside = [{"outside": True, "offset": [6, 6], "blur": 8, "color": QtGui.QColor(111, 140, 176, 105)},
                   {"outside": True, "offset": [-6, -6], "blur": 8, "color": "#FFFFFF"}]
        light_inside = [{"inside": True, "offset": [6, 6], "blur": 8, "color": "#C1D5EE"},
                  {"inside": True, "offset": [-6, -6], "blur": 8, "color": "#FFFFFF"}]
        dark_outside = [{"outside": True, "offset": [6, 6], "blur": 8, "color": QtGui.QColor(0, 0, 0, 178)},
                   {"outside": True, "offset": [-6, -6], "blur": 8, "color": QtGui.QColor(58, 58, 58, 255)}]
        dark_inside = [{"inside": True, "offset": [6, 6], "blur": 8, "color": QtGui.QColor(0, 0, 0, 178)},
                  {"inside": True, "offset": [-6, -6], "blur": 8, "color": QtGui.QColor(58, 58, 58, 255)}]

        light_style = "QPushButton{ border-radius: 15px; border: 1px solid white; width: 50px; height: 30px}"
        dark_style = "QPushButton{background: #232428; border: none; border-radius: 5px; color: rgb(4, 236, 180);} QPushButton{ border-radius: 15px; border: 1px solid #1A1A1A;; width: 50px; height: 30px}"

        # set outside and inside shadow and style
        self.outside = dark_outside
        self.inside = dark_inside

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/WingetWindow.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read() + dark_style)

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        btn_Winget = self.ui2.Winget
        btn_Choco = self.ui2.Choco
        self.ui2.Winget = BoxShadowWrapper(btn_Winget, self.inside, disable_margins=True), 5, 3

        self.ui2.verticalLayout.addWidget(btn_Choco)
        self.ui2.verticalLayout.addWidget(btn_Winget)
        self.ui2.verticalLayout.addSpacerItem(QtWidgets.QSpacerItem(100, 100, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_ScriptChoice_window)
        self.ui2.Choco.clicked.connect(self.go_to_choco_install_window)
        self.ui2.install_script.clicked.connect(self.go_to_UnifiedScriptInstall_Winget_Install)
        self.ui2.create_script.clicked.connect(self.go_to_UnifiedScriptInstall_Winget_Create)

    def show_button_ScriptChoice_window(self):
        self.hide()
        if self.w is None:
            self.w = ScriptChoiceWindowForChocoAndWinget()
        self.w.show()

    def go_to_choco_install_window(self):
        self.hide()
        if self.w is None:
            self.w = ChocoWindow()
        self.w.show()

    def go_to_UnifiedScriptInstall_Winget_Install(self):
        self.hide()
        if self.w is None:
            self.w = go_to_UnifiedScriptInstall_Winget_Install()
        self.w.show()

    def go_to_UnifiedScriptInstall_Winget_Create(self):
        self.hide()
        if self.w is None:
            self.w = go_to_UnifiedScriptInstall_Winget_Create()
        self.w.show()

class go_to_UnifiedScriptInstall_Winget_Install(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ScriptInstallationWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/UnifiedScriptInstall.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_manual_choice_window)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        os.system("cd windows-executables && exeLinkToWingetInstallScript.exe")

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = WingetWindow()
        self.w.show()

class go_to_UnifiedScriptInstall_Winget_Create(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ScriptInstallationWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/UnifiedScriptInstall.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_manual_choice_window)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        os.system("cd windows-executables && exeLinkToWingetCreateScript.exe")

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = WingetWindow()
        self.w.show()

class ChocoWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ChocoWindow()
        self.ui2.setupUi(self)

        light_outside = [{"outside": True, "offset": [6, 6], "blur": 8, "color": QtGui.QColor(111, 140, 176, 105)},
                   {"outside": True, "offset": [-6, -6], "blur": 8, "color": "#FFFFFF"}]
        light_inside = [{"inside": True, "offset": [6, 6], "blur": 8, "color": "#C1D5EE"},
                  {"inside": True, "offset": [-6, -6], "blur": 8, "color": "#FFFFFF"}]
        dark_outside = [{"outside": True, "offset": [6, 6], "blur": 8, "color": QtGui.QColor(0, 0, 0, 178)},
                   {"outside": True, "offset": [-6, -6], "blur": 8, "color": QtGui.QColor(58, 58, 58, 255)}]
        dark_inside = [{"inside": True, "offset": [6, 6], "blur": 8, "color": QtGui.QColor(0, 0, 0, 178)},
                  {"inside": True, "offset": [-6, -6], "blur": 8, "color": QtGui.QColor(58, 58, 58, 255)}]

        light_style = "QPushButton{ border-radius: 15px; border: 1px solid white; width: 50px; height: 30px}"
        dark_style = "QPushButton{background: #232428; border: none; border-radius: 5px; color: rgb(4, 236, 180);} QPushButton{ border-radius: 15px; border: 1px solid #1A1A1A;; width: 50px; height: 30px}"

        # set outside and inside shadow and style
        self.outside = dark_outside
        self.inside = dark_inside

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/ChocoWindow.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read() + dark_style)

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        btn_Winget = self.ui2.Winget
        btn_Choco = self.ui2.Choco
        self.ui2.Choco = BoxShadowWrapper(btn_Choco, self.inside, disable_margins=True), 5, 3

        self.ui2.verticalLayout.addWidget(btn_Choco)
        self.ui2.verticalLayout.addWidget(btn_Winget)
        self.ui2.verticalLayout.addSpacerItem(QtWidgets.QSpacerItem(100, 100, QSizePolicy.Minimum, QSizePolicy.Expanding))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_ScriptChoice_window)
        self.ui2.Winget.clicked.connect(self.go_to_winget_install_window)
        self.ui2.install_script.clicked.connect(self.go_to_UnifiedScriptInstall_Choco_Install)
        self.ui2.create_script.clicked.connect(self.go_to_UnifiedScriptInstall_Choco_Create)

    def show_button_ScriptChoice_window(self):
        self.hide()
        if self.w is None:
            self.w = ScriptChoiceWindowForChocoAndWinget()
        self.w.show()

    def go_to_winget_install_window(self):
        self.hide()
        if self.w is None:
            self.w = WingetWindow()
        self.w.show()

    def go_to_UnifiedScriptInstall_Choco_Install(self):
        self.hide()
        if self.w is None:
            self.w = go_to_UnifiedScriptInstall_Choco_Install()
        self.w.show()

    def go_to_UnifiedScriptInstall_Choco_Create(self):
        self.hide()
        if self.w is None:
            self.w = go_to_UnifiedScriptInstall_Choco_Create()
        self.w.show()

class go_to_UnifiedScriptInstall_Choco_Install(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ScriptInstallationWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/UnifiedScriptInstall.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_manual_choice_window)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        os.system("cd windows-executables && exeLinkToChocoInstallScript.exe")

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = ChocoWindow()
        self.w.show()

class go_to_UnifiedScriptInstall_Choco_Create(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ScriptInstallationWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/UnifiedScriptInstall.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_manual_choice_window)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        os.system("cd windows-executables && exeLinkToChocoCreateScript.exe")

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = ChocoWindow()
        self.w.show()

class BuildScriptInstallationWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ScriptInstallationWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/ScriptInstallationWindow.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_manual_choice_window)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        os.system("cd windows-executables && exeLinkToBuildScript.exe")

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = ManualChoiceWindow()
        self.w.show()

class RunBuildInstallationWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ManualInstallationWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/ManualInstallationWindow.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.show_button_manual_choice_window)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        os.system("cd windows-executables && exeLinkToRunBuild.exe")

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = ManualChoiceWindow()
        self.w.show()

class ChristitustechWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_ChristitustechWindow()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/ChristitustechWindow.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))

        self.ui2.commandLinkButton.clicked.connect(self.go_back)

        QTimer.singleShot(500, self.OnLoad)

    def OnLoad(self):
        p = subprocess.Popen(["powershell.exe",
                      "windows-executables/christitus.ps1"],
                      stdout=sys.stdout)
        p.communicate()

    def go_back(self):
        self.hide()
        if self.w is None:
            self.w = Widget()
        self.w.show()

class AhkSelectVirtualWindow(QWidget):
    def __init__(self, argGiven):
        super(AhkSelectVirtualWindow, self).__init__()

        # label = QLabel(argGiven)
        # layout.addWidget(label)

        try:
            key = winreg.OpenKey(winreg.HKEY_LOCAL_MACHINE, r"SOFTWARE\AutoHotkey")
            value, _ = winreg.QueryValueEx(key, "InstallDir")
        except FileNotFoundError:
            print("AutoHotkey is not installed or its location couldn't be found.")

        root_ahk_install_dir = value
        path_to_ahk_version_finder_1 = root_ahk_install_dir + "\\AutoHotkeyU64.exe"
        path_to_ahk_version_finder_2 = root_ahk_install_dir + "\\v2\\AutoHotkey64.exe"

        # layout = QVBoxLayout()

        self.label_path_ahk_1 = QTextEdit(path_to_ahk_version_finder_1)
        self.label_path_ahk_1.setFixedHeight(30)
        self.label_path_ahk_1.setFixedWidth(400)
        self.label_path_ahk_2 = QTextEdit(path_to_ahk_version_finder_2)
        self.label_path_ahk_2.setFixedHeight(30)
        self.label_path_ahk_2.setFixedWidth(400)
        label1 = QLabel("Autohotkey version 1 Runtime found:")
        label2 = QLabel("Autohotkey version 2 Runtime found:")
        btn1 = QPushButton('Launch script with version 1 runtime')
        btn2 = QPushButton('Launch script with version 2 runtime')

        self.central_widget = QWidget()
        self.setWindowTitle("Choose Autohotkey version")

        layout = QGridLayout()

        layout.addWidget(self.label_path_ahk_1, 0, 1)
        layout.addWidget(self.label_path_ahk_2, 1, 1)
        layout.addWidget(label1, 0, 0)
        layout.addWidget(label2, 1, 0)

        layout_for_buttons = QHBoxLayout()

        layout_for_buttons.addWidget(btn1)
        layout_for_buttons.addWidget(btn2)

        layout.addLayout(layout_for_buttons, 2, 0, 1, 2)

        btn1.clicked.connect(self.launchViaRuntime1(argGiven))
        btn2.clicked.connect(self.launchViaRuntime2(argGiven))

        self.central_widget.setLayout(layout)
        self.central_widget.show()

    def launchViaRuntime1(self, argGiven):
        def function():
            ahk_executable = self.label_path_ahk_1.toPlainText()
            ahk_script = argGiven
            subprocess.run([ahk_executable, ahk_script], shell=True)
        return function

    def launchViaRuntime2(self, argGiven):
        def function():
            ahk_executable = self.label_path_ahk_2.toPlainText()
            ahk_script = argGiven
            subprocess.run([ahk_executable, ahk_script], shell=True)
        return function

class MyButton(QtWidgets.QPushButton):
    def __init__(self, *args, **kwargs):
        super(MyButton, self).__init__(*args, **kwargs)

    def setPixmap(self, pixmap):
        self.pixmap = pixmap

    def sizeHint(self):
        parent_size = QtWidgets.QPushButton.sizeHint(self)
        return QtCore.QSize(parent_size.width() + self.pixmap.width(), max(parent_size.height(), self.pixmap.height()))

    def paintEvent(self, event):
        QtWidgets.QPushButton.paintEvent(self, event)

        pos_x = 25  # hardcoded horizontal margin
        pos_y = (self.height() - self.pixmap.height()) / 2

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
        painter.setRenderHint(QtGui.QPainter.SmoothPixmapTransform, True)
        painter.drawPixmap(pos_x, pos_y, self.pixmap)

class ScriptlessWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_Scriptless()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/Scriptless.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.scrollarea = self.ui2.scrollArea
        self.scrollarea.setWidgetResizable(True)

        widget = QWidget()
        self.scrollarea.setWidget(widget)
        self.layout_SArea = QVBoxLayout(widget)

        lib_icon_extraction_instance = classes.icon_extraction_lib

        global lst
        directory_path = "/executables"

        # Get the list of files in the directory
        lst = os.listdir(os.getcwd() + directory_path)

        if not lst:
            error_label = QLabel("Error: No files found")
            error_label.setStyleSheet("color: white; qproperty-alignment: AlignCenter;");
            self.layout_SArea.addWidget(error_label)
            print("List is empty")

        for i in range(len(lst)):
            # btn = QPushButton(lst[i])
            pb = MyButton('                 ' + lst[i])
            layoutForButton = QHBoxLayout()

            file = os.getcwd() + "\\executables\\" + lst[i]
            # print(file)

            if self.is_exe(file):
                small_icon = lib_icon_extraction_instance.extract_icon(file, lib_icon_extraction_instance.IconSize.SMALL)
                large_icon = lib_icon_extraction_instance.extract_icon(file, lib_icon_extraction_instance.IconSize.LARGE)
                img_small = lib_icon_extraction_instance.win32_icon_to_image(small_icon, lib_icon_extraction_instance.IconSize.SMALL)
                img_large = lib_icon_extraction_instance.win32_icon_to_image(large_icon, lib_icon_extraction_instance.IconSize.LARGE)
                img_small.save("classes/hashIcons/python_small_" + lst[i] + ".bmp")
                img_large.save("classes/hashIcons/python_large_" + lst[i] + ".bmp")
                pb.pressed.connect(self.exe_button_clicked(i))
                path = r"classes/hashIcons/python_large_" + lst[i] + ".bmp"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_link(file):
                pb.pressed.connect(self.lnk_button_clicked(i))
                path = r"Icon/windows-arrow-icon-16.jpg"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_script_ahk(file):
                pb.pressed.connect(self.script_ahk_button_clicked(i))
                path = r"Icon/ahk_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_script_python(file):
                pb.pressed.connect(self.script_python_button_clicked(i))
                path = r"Icon/python_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_url(file):
                pb.pressed.connect(self.url_button_clicked(i))
                path = r"Icon/url_link.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_power_shell(file):
                pb.pressed.connect(self.script_power_shell_button_clicked(i))
                path = r"Icon/PowerShell_5.0_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_cmd(file):
                pb.pressed.connect(self.script_cmd_button_clicked(i))
                path = r"Icon/cmd_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_bat(file):
                pb.pressed.connect(self.script_bat_button_clicked(i))
                path = r"Icon/cmd_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)


        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))
        self.ui2.commandLinkButton.clicked.connect(self.go_back)
        self.ui2.executeAll.clicked.connect(self.executeAll(lst))

    def exe_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        subprocess.run(pathToFile)

    def exe_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            subprocess.run(pathToFile)
        return test

    def lnk_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.startfile (pathToFile)

    def lnk_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.startfile (pathToFile)
        return test

    def url_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.startfile (pathToFile)

    def url_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.startfile (pathToFile)
        return test

    def script_ahk_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        self.w = AhkSelectVirtualWindow(pathToFile)

    def script_ahk_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            self.w = AhkSelectVirtualWindow(pathToFile)
        return test

    def script_python_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        subprocess.run(["python", pathToFile])

    def script_python_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            subprocess.run(["python", pathToFile])
        return test

    def script_power_shell_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        p = subprocess.Popen(["powershell.exe", pathToFile], stdout=sys.stdout)
        p.communicate()

    def script_power_shell_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            p = subprocess.Popen(["powershell.exe", pathToFile], stdout=sys.stdout)
            p.communicate()
        return test

    def script_cmd_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.system(pathToFile)

    def script_cmd_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.system(pathToFile)
        return test

    def script_bat_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.system(pathToFile)

    def script_bat_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.system(pathToFile)
        return test

    def go_back(self):
        self.hide()
        if self.w is None:
            self.w = Widget()
        self.w.show()

    def is_exe(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.exe'

    def is_link(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.lnk'

    def is_url(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.url'

    def is_script_ahk(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.ahk'

    def is_script_python(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.py'

    def is_power_shell(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.ps1'

    def is_cmd(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.cmd'

    def is_bat(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.bat'

    def executeAll(self, lst):
        def test():
            for i in range(len(lst)):
                pathToFile = os.getcwd() + "\\executables\\" + lst[i]

                if self.is_exe(lst[i]):
                    self.exe_function(i)
                elif self.is_link(lst[i]):
                    self.lnk_function(i)
                elif self.is_url(lst[i]):
                    self.url_function(i)
                elif self.is_script_ahk(lst[i]):
                    self.script_ahk_function(i)
                elif self.is_script_python(lst[i]):
                    self.script_python_function(i)
                elif self.is_power_shell(lst[i]):
                    self.script_power_shell_function(i)
                elif self.is_cmd(lst[i]):
                    self.script_cmd_function(i)
                elif self.is_bat(lst[i]):
                    self.script_bat_function(i)
        return test

class SetupWindow(QWidget):
    def __init__(self):
        super().__init__()

        self.ui2 = Ui_Scriptless()
        self.ui2.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/Scriptless.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.scrollarea = self.ui2.scrollArea
        self.scrollarea.setWidgetResizable(True)

        widget = QWidget()
        self.scrollarea.setWidget(widget)
        self.layout_SArea = QVBoxLayout(widget)

        lib_icon_extraction_instance = classes.icon_extraction_lib

        self.label = self.ui2.label
        self.label.setText("Choose a setup action to perform:")

        global lst
        directory_path = "/setup"

        # Get the list of files in the directory
        lst = os.listdir(os.getcwd() + directory_path)

        if not lst:
            error_label = QLabel("Error: No files found")
            error_label.setStyleSheet("color: white; qproperty-alignment: AlignCenter;");
            self.layout_SArea.addWidget(error_label)
            print("List is empty")

        for i in range(len(lst)):
            # btn = QPushButton(lst[i])
            pb = MyButton('                 ' + lst[i])
            layoutForButton = QHBoxLayout()

            file = os.getcwd() + "\\executables\\" + lst[i]
            # print(file)

            if self.is_exe(file):
                small_icon = lib_icon_extraction_instance.extract_icon(file, lib_icon_extraction_instance.IconSize.SMALL)
                large_icon = lib_icon_extraction_instance.extract_icon(file, lib_icon_extraction_instance.IconSize.LARGE)
                img_small = lib_icon_extraction_instance.win32_icon_to_image(small_icon, lib_icon_extraction_instance.IconSize.SMALL)
                img_large = lib_icon_extraction_instance.win32_icon_to_image(large_icon, lib_icon_extraction_instance.IconSize.LARGE)
                img_small.save("classes/hashIcons/python_small_" + lst[i] + ".bmp")
                img_large.save("classes/hashIcons/python_large_" + lst[i] + ".bmp")
                pb.pressed.connect(self.exe_button_clicked(i))
                path = r"classes/hashIcons/python_large_" + lst[i] + ".bmp"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_link(file):
                pb.pressed.connect(self.lnk_button_clicked(i))
                path = r"Icon/windows-arrow-icon-16.jpg"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_script_ahk(file):
                pb.pressed.connect(self.script_ahk_button_clicked(i))
                path = r"Icon/ahk_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_script_python(file):
                pb.pressed.connect(self.script_python_button_clicked(i))
                path = r"Icon/python_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_url(file):
                pb.pressed.connect(self.url_button_clicked(i))
                path = r"Icon/url_link.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_power_shell(file):
                pb.pressed.connect(self.script_power_shell_button_clicked(i))
                path = r"Icon/PowerShell_5.0_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_cmd(file):
                pb.pressed.connect(self.script_cmd_button_clicked(i))
                path = r"Icon/cmd_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

            if self.is_bat(file):
                pb.pressed.connect(self.script_bat_button_clicked(i))
                path = r"Icon/cmd_icon.png"
                pixmap = QtGui.QPixmap(path).scaled(40, 40, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)
                pb.setPixmap(pixmap)

                if(i%2==0):
                    pb.setStyleSheet("QPushButton { background-color: #333A40; text-align: left;} QPushButton:hover { background-color: #5C6166; } QPushButton:pressed { background-color: #474E53;}");
                else:
                    pb.setStyleSheet("QPushButton { text-align: left;} QPushButton:pressed { background-color: #3d4245;} text-align: left;");

                self.layout_SArea.addWidget(pb)

        self.ui2.commandLinkButton.setIcon(QIcon("assets/BackArrowAndImage.png"))
        self.ui2.commandLinkButton.clicked.connect(self.go_back)
        self.ui2.executeAll.clicked.connect(self.executeAll(lst))

    def exe_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        subprocess.run(pathToFile)

    def exe_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            subprocess.run(pathToFile)
        return test

    def lnk_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.startfile (pathToFile)

    def lnk_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.startfile (pathToFile)
        return test

    def url_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.startfile (pathToFile)

    def url_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.startfile (pathToFile)
        return test

    def script_ahk_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        self.w = AhkSelectVirtualWindow(pathToFile)

    def script_ahk_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            self.w = AhkSelectVirtualWindow(pathToFile)
        return test

    def script_python_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        subprocess.run(["python", pathToFile])

    def script_python_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            subprocess.run(["python", pathToFile])
        return test

    def script_power_shell_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        p = subprocess.Popen(["powershell.exe", pathToFile], stdout=sys.stdout)
        p.communicate()

    def script_power_shell_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            p = subprocess.Popen(["powershell.exe", pathToFile], stdout=sys.stdout)
            p.communicate()
        return test

    def script_cmd_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.system(pathToFile)

    def script_cmd_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.system(pathToFile)
        return test

    def script_bat_function(self, i):
        pathToFile = os.getcwd() + "\\executables\\" + lst[i]
        os.system(pathToFile)

    def script_bat_button_clicked(self, i):
        def test():
            pathToFile = os.getcwd() + "\\executables\\" + lst[i]
            os.system(pathToFile)
        return test

    def go_back(self):
        self.hide()
        if self.w is None:
            self.w = Widget()
        self.w.show()

    def is_exe(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.exe'

    def is_link(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.lnk'

    def is_url(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.url'

    def is_script_ahk(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.ahk'

    def is_script_python(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.py'

    def is_power_shell(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.ps1'

    def is_cmd(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.cmd'

    def is_bat(self, file_path):
        _, file_extension = os.path.splitext(file_path)
        return file_extension.lower() == '.bat'

    def executeAll(self, lst):
        def test():
            for i in range(len(lst)):
                pathToFile = os.getcwd() + "\\executables\\" + lst[i]

                if self.is_exe(lst[i]):
                    self.exe_function(i)
                elif self.is_link(lst[i]):
                    self.lnk_function(i)
                elif self.is_url(lst[i]):
                    self.url_function(i)
                elif self.is_script_ahk(lst[i]):
                    self.script_ahk_function(i)
                elif self.is_script_python(lst[i]):
                    self.script_python_function(i)
                elif self.is_power_shell(lst[i]):
                    self.script_power_shell_function(i)
                elif self.is_cmd(lst[i]):
                    self.script_cmd_function(i)
                elif self.is_bat(lst[i]):
                    self.script_bat_function(i)
        return test

class Widget(QWidget):
    def __init__(self):
        super(Widget, self).__init__()

        self.ui1 = Ui_Widget1()
        self.ui1.setupUi(self)

        self.setWindowTitle('InstallationWizard')
        self.setFixedSize(800,600)
        self.w = None  # No external window yet.
        qss="qssStyles/style.qss"
        with open(qss,"r") as fh:
            self.setStyleSheet(fh.read())

        icon=QIcon("icon/OIG3256x256.ico")
        self.setWindowIcon(icon)

        self.ui1.Setup.clicked.connect(self.show_Setup_window)
        self.ui1.button_script.clicked.connect(self.show_button_ScriptChoice_window)
        self.ui1.button_christitustech.clicked.connect(self.show_button_christitustech_window)
        self.ui1.button_manual.clicked.connect(self.show_button_manual_choice_window)
        self.ui1.Scriptless.clicked.connect(self.show_Scripless_window)

    def show_button_christitustech_window(self):
        self.hide()
        if self.w is None:
            self.w = ChristitustechWindow()
        self.w.show()

    def show_button_ScriptChoice_window(self):
        self.hide()
        if self.w is None:
            self.w = ScriptChoiceWindowForChocoAndWinget()
        self.w.show()

    def show_button_manual_choice_window(self):
        self.hide()
        if self.w is None:
            self.w = ManualChoiceWindow()
        self.w.show()

    def show_Scripless_window(self):
        self.hide()
        if self.w is None:
            self.w = ScriptlessWindow()
        self.w.show()

    def show_Setup_window(self):
        self.hide()
        if self.w is None:
            self.w = SetupWindow()
        self.w.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)

    w = Widget()
    w.show()

    sys.exit(app.exec())
