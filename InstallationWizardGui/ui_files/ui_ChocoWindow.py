# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ChocoWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCommandLinkButton, QFrame, QHBoxLayout,
    QLabel, QLayout, QPushButton, QSizePolicy,
    QSpacerItem, QVBoxLayout, QWidget)

class Ui_ChocoWindow(object):
    def setupUi(self, ChocoWindow):
        if not ChocoWindow.objectName():
            ChocoWindow.setObjectName(u"ChocoWindow")
        ChocoWindow.resize(800, 600)
        self.commandLinkButton = QCommandLinkButton(ChocoWindow)
        self.commandLinkButton.setObjectName(u"commandLinkButton")
        self.commandLinkButton.setGeometry(QRect(10, 10, 40, 50))
        icon = QIcon()
        icon.addFile(u"../../../Desktop/DesignElementsForGui/BackArrowAndImage.png", QSize(), QIcon.Normal, QIcon.Off)
        self.commandLinkButton.setIcon(icon)
        self.commandLinkButton.setIconSize(QSize(110, 30))
        self.commandLinkButton.setAutoDefault(False)
        self.commandLinkButton.setDefault(False)
        self.layoutWidget = QWidget(ChocoWindow)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(0, 70, 801, 461))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.Choco = QPushButton(self.layoutWidget)
        self.Choco.setObjectName(u"Choco")

        self.verticalLayout.addWidget(self.Choco)

        self.Winget = QPushButton(self.layoutWidget)
        self.Winget.setObjectName(u"Winget")

        self.verticalLayout.addWidget(self.Winget)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        font = QFont()
        font.setPointSize(1)
        self.label.setFont(font)
        self.label.setAutoFillBackground(False)
        self.label.setFrameShadow(QFrame.Plain)
        self.label.setLineWidth(0)

        self.horizontalLayout.addWidget(self.label)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_7)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.verticalLayout_2.setContentsMargins(100, -1, 100, -1)
        self.install_script = QPushButton(self.layoutWidget)
        self.install_script.setObjectName(u"install_script")

        self.verticalLayout_2.addWidget(self.install_script)

        self.create_script = QPushButton(self.layoutWidget)
        self.create_script.setObjectName(u"create_script")

        self.verticalLayout_2.addWidget(self.create_script)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)


        self.retranslateUi(ChocoWindow)

        QMetaObject.connectSlotsByName(ChocoWindow)
    # setupUi

    def retranslateUi(self, ChocoWindow):
        ChocoWindow.setWindowTitle(QCoreApplication.translate("ChocoWindow", u"Form", None))
        self.commandLinkButton.setText("")
        self.Choco.setText(QCoreApplication.translate("ChocoWindow", u"Choco", None))
        self.Winget.setText(QCoreApplication.translate("ChocoWindow", u"Winget", None))
        self.label.setText("")
        self.install_script.setText(QCoreApplication.translate("ChocoWindow", u"Install script", None))
        self.create_script.setText(QCoreApplication.translate("ChocoWindow", u"Create script", None))
    # retranslateUi

