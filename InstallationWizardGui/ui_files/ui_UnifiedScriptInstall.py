# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'UnifiedScriptInstall.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCommandLinkButton, QHBoxLayout, QLabel,
    QLayout, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_UnifiedScriptInstall(object):
    def setupUi(self, UnifiedScriptInstall):
        if not UnifiedScriptInstall.objectName():
            UnifiedScriptInstall.setObjectName(u"UnifiedScriptInstall")
        UnifiedScriptInstall.resize(800, 600)
        self.commandLinkButton = QCommandLinkButton(UnifiedScriptInstall)
        self.commandLinkButton.setObjectName(u"commandLinkButton")
        self.commandLinkButton.setGeometry(QRect(10, 10, 40, 50))
        icon = QIcon()
        icon.addFile(u"../../../Desktop/DesignElementsForGui/BackArrowAndImage.png", QSize(), QIcon.Normal, QIcon.Off)
        self.commandLinkButton.setIcon(icon)
        self.commandLinkButton.setIconSize(QSize(110, 30))
        self.commandLinkButton.setAutoDefault(False)
        self.commandLinkButton.setDefault(False)
        self.verticalLayoutWidget = QWidget(UnifiedScriptInstall)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(0, 70, 801, 421))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(0, 100, 0, 100)
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.horizontalSpacer_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(200, -1, 200, -1)
        self.horizontalSpacer_3 = QSpacerItem(60, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)

        self.label = QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(u"label")
        self.label.setWordWrap(True)

        self.horizontalLayout.addWidget(self.label)

        self.horizontalSpacer_4 = QSpacerItem(60, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.horizontalSpacer)

        self.pushButton = QPushButton(UnifiedScriptInstall)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(270, 30, 80, 24))

        self.retranslateUi(UnifiedScriptInstall)

        QMetaObject.connectSlotsByName(UnifiedScriptInstall)
    # setupUi

    def retranslateUi(self, UnifiedScriptInstall):
        UnifiedScriptInstall.setWindowTitle(QCoreApplication.translate("UnifiedScriptInstall", u"Form", None))
        self.commandLinkButton.setText("")
        self.label.setText(QCoreApplication.translate("UnifiedScriptInstall", u"A separate script process is launched", None))
        self.pushButton.setText(QCoreApplication.translate("UnifiedScriptInstall", u"PushButton", None))
    # retranslateUi

