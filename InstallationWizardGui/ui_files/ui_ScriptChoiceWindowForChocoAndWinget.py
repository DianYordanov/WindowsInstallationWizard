# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ScriptChoiceWindowForChocoAndWinget.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCommandLinkButton, QHBoxLayout, QLabel,
    QLayout, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_ScriptChoiceWindowForChocoAndWinget(object):
    def setupUi(self, ScriptChoiceWindowForChocoAndWinget):
        if not ScriptChoiceWindowForChocoAndWinget.objectName():
            ScriptChoiceWindowForChocoAndWinget.setObjectName(u"ScriptChoiceWindowForChocoAndWinget")
        ScriptChoiceWindowForChocoAndWinget.resize(800, 600)
        self.commandLinkButton = QCommandLinkButton(ScriptChoiceWindowForChocoAndWinget)
        self.commandLinkButton.setObjectName(u"commandLinkButton")
        self.commandLinkButton.setGeometry(QRect(10, 10, 40, 50))
        icon = QIcon()
        icon.addFile(u"../../../Desktop/DesignElementsForGui/BackArrowAndImage.png", QSize(), QIcon.Normal, QIcon.Off)
        self.commandLinkButton.setIcon(icon)
        self.commandLinkButton.setIconSize(QSize(110, 30))
        self.commandLinkButton.setAutoDefault(False)
        self.commandLinkButton.setDefault(False)
        self.layoutWidget = QWidget(ScriptChoiceWindowForChocoAndWinget)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(0, 69, 801, 461))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(0, 100, 0, 100)
        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setWordWrap(True)

        self.verticalLayout.addWidget(self.label)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.horizontalSpacer_2)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_6)

        self.Choco = QPushButton(self.layoutWidget)
        self.Choco.setObjectName(u"Choco")

        self.horizontalLayout_2.addWidget(self.Choco)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_5)

        self.Winget = QPushButton(self.layoutWidget)
        self.Winget.setObjectName(u"Winget")

        self.horizontalLayout_2.addWidget(self.Winget)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_7)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.horizontalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)


        self.retranslateUi(ScriptChoiceWindowForChocoAndWinget)

        QMetaObject.connectSlotsByName(ScriptChoiceWindowForChocoAndWinget)
    # setupUi

    def retranslateUi(self, ScriptChoiceWindowForChocoAndWinget):
        ScriptChoiceWindowForChocoAndWinget.setWindowTitle(QCoreApplication.translate("ScriptChoiceWindowForChocoAndWinget", u"Form", None))
        self.commandLinkButton.setText("")
        self.label.setText(QCoreApplication.translate("ScriptChoiceWindowForChocoAndWinget", u"Choose a script type:", None))
        self.Choco.setText(QCoreApplication.translate("ScriptChoiceWindowForChocoAndWinget", u"Choco", None))
        self.Winget.setText(QCoreApplication.translate("ScriptChoiceWindowForChocoAndWinget", u"Winget", None))
    # retranslateUi

