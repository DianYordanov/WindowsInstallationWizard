# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ManualChoiceWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCommandLinkButton, QHBoxLayout, QLabel,
    QLayout, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)

class Ui_ManualChoiceWindow(object):
    def setupUi(self, ManualChoiceWindow):
        if not ManualChoiceWindow.objectName():
            ManualChoiceWindow.setObjectName(u"ManualChoiceWindow")
        ManualChoiceWindow.resize(800, 600)
        self.commandLinkButton = QCommandLinkButton(ManualChoiceWindow)
        self.commandLinkButton.setObjectName(u"commandLinkButton")
        self.commandLinkButton.setGeometry(QRect(10, 10, 40, 50))
        icon = QIcon()
        icon.addFile(u"../../../Desktop/DesignElementsForGui/BackArrowAndImage.png", QSize(), QIcon.Normal, QIcon.Off)
        self.commandLinkButton.setIcon(icon)
        self.commandLinkButton.setIconSize(QSize(110, 30))
        self.commandLinkButton.setAutoDefault(False)
        self.commandLinkButton.setDefault(False)
        self.layoutWidget = QWidget(ManualChoiceWindow)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(0, 70, 801, 461))
        self.horizontalLayout = QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setSizeConstraint(QLayout.SetDefaultConstraint)
        self.verticalLayout.setContentsMargins(0, 100, 0, 100)
        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setWordWrap(True)

        self.verticalLayout.addWidget(self.label)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.horizontalSpacer_2)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_6)

        self.BuildScript = QPushButton(self.layoutWidget)
        self.BuildScript.setObjectName(u"BuildScript")

        self.horizontalLayout_2.addWidget(self.BuildScript)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_5)

        self.RunBuild = QPushButton(self.layoutWidget)
        self.RunBuild.setObjectName(u"RunBuild")

        self.horizontalLayout_2.addWidget(self.RunBuild)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_7)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.verticalLayout.addItem(self.horizontalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)


        self.retranslateUi(ManualChoiceWindow)

        QMetaObject.connectSlotsByName(ManualChoiceWindow)
    # setupUi

    def retranslateUi(self, ManualChoiceWindow):
        ManualChoiceWindow.setWindowTitle(QCoreApplication.translate("ManualChoiceWindow", u"Form", None))
        self.commandLinkButton.setText("")
        self.label.setText(QCoreApplication.translate("ManualChoiceWindow", u"Which installation method do you choose?", None))
        self.BuildScript.setText(QCoreApplication.translate("ManualChoiceWindow", u"Build script", None))
        self.RunBuild.setText(QCoreApplication.translate("ManualChoiceWindow", u"Run build", None))
    # retranslateUi

