﻿#Requires AutoHotkey v2

path_to_hashIcons := A_WorkingDir . "\classes\hashIcons"

DirDelete path_to_hashIcons, true
DirCreate path_to_hashIcons

TrayTip "#1", "Refreshed " . "\classes\hashIcons"
Sleep 3000   ; Let it display for 3 seconds.
HideTrayTip

path_to_executables := A_WorkingDir . "\executables"

DirDelete path_to_executables, true
DirCreate path_to_executables

TrayTip "#2", "Refreshed " . "\executables"
Sleep 3000   ; Let it display for 3 seconds.
HideTrayTip

path_to_setup := A_WorkingDir . "\setup"

DirDelete path_to_setup, true
DirCreate path_to_setup

TrayTip "#3", "Refreshed " . "\setup"
Sleep 3000   ; Let it display for 3 seconds.
HideTrayTip

; Copy this function into your script to use it.
HideTrayTip() {
    TrayTip  ; Attempt to hide it the normal way.
    if SubStr(A_OSVersion,1,3) = "10." {
        A_IconHidden := true
        Sleep 200  ; It may be necessary to adjust this sleep.
        A_IconHidden := false
    }
}