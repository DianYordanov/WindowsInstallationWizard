﻿#Requires AutoHotkey v2
cl := DllCall( "GetCommandLine", "str" )
cl_array := StrSplit(cl, "`" `"")
ScriptDir := A_ScriptDir . "/ahk_version_2_dir"
If FileExist(ScriptDir)
	FileDelete "ahk_version_2_dir"
FileAppend(SubStr(cl_array[1], 1, StrLen(cl_array[1])), "ahk_version_2_dir")
