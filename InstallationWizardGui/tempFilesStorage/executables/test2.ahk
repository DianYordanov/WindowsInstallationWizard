﻿#Requires AutoHotkey v1.1.33.10
;┌─────────────┐
;│   Hotkeys   │
;╘═════════════╛
F1::
    NewTrayCountdown := 0 ;How long to start the new countdown for?
    SetTimer, TrayCountdown, 100 ;Trigger an update every 200ms
return
;┌───────────────┐
;│   Functions   │
;╘═══════════════╛
TrayCountdown:
    ; If it's a new countdown, initialise things
    if (NewTrayCountdown) {
        ; Store the length, and clear the new countdown var so we don't initialise again
        TrayCountdownLength := NewTrayCountdown
        NewTrayCountdown := ""
        ; Store start-time, and calculate ending tick we'll stop at
        TrayCountdownStart := A_TickCount
        TrayCountdownEnd := A_TickCount + NewTrayCountdown
        ; Do the stuff that happens on first tick
        TrayTip, , Successfully created  a chocolatey custom install script
        ; Exit out, until next update
        return
    }
    ; If we haven't reached the ending tick, it's a normal update
    if (A_TickCount <= TrayCountdownEnd) {
        ; Calculate some stuff for displaying
        CountdownElapsed := A_TickCount - TrayCountdownStart
        ; Update text
        TrayTip, , %CountdownElapsed% / %CountdownLength%
        ; Exit out, until next update
        return
    }
    ; If we've reached the target tick, it's the last update
    if (A_TickCount > TrayCountDownEnd) {
        ; Stop timer
        SetTimer, , Off
        ; Update text
        TrayTip, , Successfully created  a chocolatey custom install script
        ; Exit out
        return
    }
return