;Change this file to customize zip2exe generated installers with a modern interface

!include "MUI2.nsh"

!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!insertmacro MUI_LANGUAGE "English"

; These are the programs that are needed by ACME Suite.
Section -Prerequisites
    SetOutPath $INSTDIR\Prerequisites
    MessageBox MB_YESNO "Install AHK2?" /SD IDYES IDNO endAHK2
        ExecWait "$INSTDIR\Prerequisites\AutoHotkey_2.0.12_setup.exe"
        Goto endAHK2
    endAHK2:
    MessageBox MB_YESNO "Install AHK1?" /SD IDYES IDNO endAHK1
        ExecWait "$INSTDIR\Prerequisites\AutoHotkey_1.1.37.02_setup.exe"
        Goto endAHK1
    endAHK1:
    MessageBox MB_YESNO "Install Python?" /SD IDYES IDNO endPython
        ExecWait "$INSTDIR\Prerequisites\python-3.12.2-amd64.exe"
        Goto endPython
    endPython:
    MessageBox MB_YESNO "Install Chocolatey?" /SD IDYES IDNO endChocolatey
        ExpandEnvStrings $0 "%COMSPEC%"
        ExecShell "" '"$0"' "/C powershell -ExecutionPolicy Bypass $INSTDIR\Prerequisites\InstallChocolatey.ps1 -FFFeatureOff" SW_HIDE
        Goto endChocolatey
    endChocolatey:
    MessageBox MB_YESNO "Install winget?" /SD IDYES IDNO endwinget
        ExpandEnvStrings $0 "%COMSPEC%"
        ExecShell "" '"$0"' "/C powershell -ExecutionPolicy Bypass $INSTDIR\Prerequisites\winget-install.ps1 -FFFeatureOff" SW_HIDE
        Goto endwinget
    endwinget:
SectionEnd

Section
        SetOutPath $INSTDIR
        # create the uninstaller
        WriteUninstaller "$INSTDIR\uninstall.exe"

        CreateShortcut "$SMPROGRAMS\WindowsInstallationWizard.lnk" "$INSTDIR\WindowsInstallationWizard\WindowsInstallationWizard.exe"
SectionEnd

# uninstaller section start
Section "uninstall"
 
    # Remove the link from the start menu
    Delete "$SMPROGRAMS\WindowsInstallationWizard.lnk"
 
    # Delete the uninstaller
    Delete $INSTDIR\uninstaller.exe
 
    RMDir  /r $INSTDIR\WindowsInstallationWizard
# uninstaller section end
SectionEnd